import React from 'react';
import { render } from '@testing-library/react';

import Product from './index';

const product = {
  id: 1,
  name: "Furadeira de Impacto",
  picture: "https://cdn.leroymerlin.com.br/products/0.jpg",
  price: {
    to: {
      integers: "965",
      decimals: "90"
    },
    from: {
      integers: "299",
      decimals: "99"
    },
  },
  unit: "cada",
  installments: {
    amount: 8,
    value: "120,74"
  },
  tag: {
   label: "Exclusivo" 
  },
  offer: {
    label: "Oferta",
    value: 11
  },
  url: "https://www.leroymerlin.com.br/",
}

it('should be able to show product item', () => {
  render(
    <Product
      className="products__item"
      product={product}
      key={product.id}
      addToCart={() => () => {}}
    />
  );
});