import React from 'react';
import './Product.css';

import imageDefault from '../../../assets/default.svg';

interface IPrice {
  to: {
    integers: string;
    decimals: string;
  };
  from: {
    integers: string;
    decimals: string;
  };
}
export interface IProduct {
  id: number;
  name: string;
  picture: string;
  price: IPrice;
  unit: string;
  installments: {
    amount: number;
    value: string;
  };
  tag: {
   label: string;
  };
  offer: {
    label: string;
    value: number;
  };
  url: string;
}
interface Props {
  className: string;
  product: IProduct;
  addToCart: Function;
}

function Product(props: Props) {
  const { className, product, addToCart } = props;  
  const picture = product.picture === null ? imageDefault : product.picture;
  const tagIsNull = product.tag === null;
  
  const offerIsNull = product.offer === null;
  const installmentsInNull = product.installments === null;
  
  const priceToIsNull = product.price.to === null;
  const priceFromIsNull = product.price.from === null;
  
  const priceFrom = `${product?.price?.from?.integers},${product?.price?.from?.decimals}`;
  const priceTo = `${product?.price?.to?.integers},${product?.price?.to?.decimals}`;
  
  const amount = product?.installments?.amount;
  const value = product?.installments?.value;
  
  return (      
    <li className={className}>
      <div className="thumb">
        <div className={`thumb__tag ${tagIsNull ? 'thumb__tag--hide': ''}`}>{product?.tag?.label}</div>
        <img className="thumb__picture" src={picture} alt={product?.name} />
        <div className={`thumb__offer ${offerIsNull ? 'thumb__offer--hide': ''}`}>
          <span className="thumb__label">{product?.offer?.label}</span>
          <span className="thumb__value">{product?.offer?.value}%</span>
        </div>
      </div>
      <div className="info">
        <p className="info__name">{product?.name}</p> 
        <button className="info__button" onClick={() => addToCart({
          id: product.id,
          name: product.name,
          picture: product.picture,
          price: priceTo.replace(',', '.'),
          quantity: 1
        })}>
          <span className="info__span">Adicionar ao carrinho</span>
        </button>
      </div>
      <div className={`price ${!installmentsInNull ? 'price--promotional': ''}`}>
        <p className={`price__to ${priceToIsNull ? 'price__to--hide': ''}`}>
          R$ {priceTo}
        </p>
        <p className={`price__from ${priceFromIsNull ? 'price__from--hide': ''}`}>
          R$ {priceFrom} {product?.unit}
        </p>
        <p className={`price__installments ${installmentsInNull ? 'price__installments--hide': ''}`}>
          {amount}x de R${value} s/juros
        </p>
      </div>
    </li>
  );
}

export default Product;