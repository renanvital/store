import React from 'react';
import { render } from '@testing-library/react';

import CartList from './index';
import { CartProvider } from '../../../context/CartContext';

it('should be able to show cart list empty', () => {
  
  const { getByText, getByAltText } = render(
    <CartProvider>
      <CartList />
    </CartProvider>
  );

  const title = getByText('Carrinho');
  const text = getByText('Carrinho vazio!');
  const image = getByAltText('Carrinho vazio!');

  expect(title).toBeInTheDocument();
  expect(text).toBeInTheDocument();
  expect(image).toBeInTheDocument();
});