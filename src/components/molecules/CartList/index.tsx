import React from 'react';
import './CartList.css';

import Cart from '../Cart';
import { useCart } from '../../../context/CartContext';

import cartEmptyImage from '../../../assets/cart-empty.png';

function CartList() {
  const {
    isOpen,
    closeCart,
    products,
    totalProducts,
    shipping,
    calculateShipping,
    CEP,
    setCEP,
    checkout,
    subTotal,
    subTotalWithShipping
  } = useCart();
  
  return (
    <div className={`cartModal ${ isOpen ? 'cartModal--open': ''}`}>
      <div className="cart">        
        <h2 className="cart__title">Carrinho</h2>
        {(totalProducts === 0) ?
          <> 
            <p className="cart__message">
              Carrinho vazio!
            </p>
            <img className="cart__image" src={cartEmptyImage} alt="Carrinho vazio!" />
            <button className="cart__back" onClick={() => closeCart()}>
              Ir para loja
            </button>
          </>
        :
          <>
            <ul className="cart__list">
              {products.map(item => 
                <Cart key={item.id} product={item} />
                )}
            </ul>
            <div className="cart__shipping">
              <div>
                CEP: <input className="cart__input" type="text" value={CEP} placeholder="00000-000" maxLength={9} onChange={event => setCEP(event.target.value)} />
                <button className="cart__cep" onClick={() => calculateShipping(CEP)}>
                  Calcular frete
                </button>
              </div>
              <div className="cart__price">
                <small>Subtotal: R$ {subTotal.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })}</small>
                <small>{shipping !== '' && `Frete: R$ ${shipping}`}</small>
                <strong>{shipping !== '' && `Total: R$ ${subTotalWithShipping.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })}`}</strong>
              </div>
            </div>
            <button className="cart__checkout" onClick={() => checkout()}>
              Finalizar compra
            </button>
          </>
        }  
      </div>
      <button className="cart__close" onClick={() => closeCart()}>X</button> 
    </div>
  );
}

export default CartList;