import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import CartHeader from './index';
import { CartProvider } from '../../../context/CartContext';

it('should be able to show cart header', () => {
  const { getByText, getByAltText } = render(<CartHeader />);

  const text = getByText('Carrinho');
  const image = getByAltText('Carrinho');

  expect(text).toBeInTheDocument();  
  expect(image).toBeInTheDocument();
});

it('should be able to click on cart header', () => {
  const { getByText, getByAltText } = render(
    <CartProvider>
      <CartHeader />
    </CartProvider>
  );
  
  const clickOnText = fireEvent.click(getByText('Carrinho'));
  const clickOnIcon = fireEvent.click(getByAltText('Carrinho'));

  expect(clickOnText).toBeTruthy();
  expect(clickOnIcon).toBeTruthy();
});

it('should be able start count with zero', () => {
  const { getByText } = render(
    <CartProvider>
      <CartHeader />
    </CartProvider>
  );
  
  const count = getByText('0');

  expect(count).toBeInTheDocument();
});