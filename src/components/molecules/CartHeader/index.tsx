import React from 'react';
import './CartHeader.css';

import cartImage from '../../../assets/cart.svg';
import { useCart } from '../../../context/CartContext';

function CartHeader() {  
  const { totalProducts,  openCart } = useCart();

  return (
    <div className="cartHeader" onClick={() => openCart()}>
      <span className="cartHeader__text">Carrinho</span>
      <img className="cartHeader__image" src={cartImage} alt="Carrinho" /> 
      <span className={`cartHeader__count ${totalProducts > 0 ? 'cartHeader__count--active' : ''}`}>
        {totalProducts}
      </span>
    </div>
  );
}

export default CartHeader;