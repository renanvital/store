import React from 'react';
import { useCart } from '../../../context/CartContext';
import './Cart.css';

import remove from '../../../assets/remove.svg';
import plus from '../../../assets/plus.svg';
import minus from '../../../assets/minus.svg';
interface Product {
  id: number;
  name: string;
  picture: string;
  price: string;
  quantity: number;
}
interface Props {
  product: Product;
}

function Cart(props: Props) {
  const { product } = props;
  const {
    incrementProduct,
    decrementProduct,
    removeProduct
  } = useCart();
  
  return (      
    <li className="cart__item">
      <button className="item__delete" onClick={() => removeProduct(product.id)}>
        <img src={remove} alt="Remover do Carrinho" />
      </button>
      <img className="item__image" src={product.picture} alt={product.name} width={50} />
      <p className="item__name">{product.name}</p>  
      <div className="item__quantity">
        <button className="item__plus" type="button" name="button" onClick={() => incrementProduct(product.id)}>
          <img src={plus} alt="Adicionar um Item" />
        </button>
        <input className="item__input" type="text" name="name" value={product.quantity} readOnly />
        <button className="item__minus" type="button" name="button" onClick={() => decrementProduct(product.id)}>
          <img src={minus} alt="Diminuir um Item" />
        </button>
      </div>
      <div className="item__price">R$ {product?.price}</div>
    </li>
  );
}

export default Cart;