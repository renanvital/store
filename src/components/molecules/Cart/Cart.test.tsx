import React from 'react';
import { render } from '@testing-library/react';

import Cart from './index';

const product = {
  id: 1,
  name: "Furadeira de Impacto",
  picture: "https://cdn.leroymerlin.com.br/products/0.jpg",
  price: "100",
  quantity: 1
};

it('should be able to show cart item', () => {
  const { getByText, getByAltText, getByDisplayValue } = render(<Cart key={product.id} product={product} />);

  const name = getByText('Furadeira de Impacto');
  const picture = getByAltText('Furadeira de Impacto');
  const price = getByText('R$ 100');
  const quantity = getByDisplayValue('1');

  expect(name).toBeInTheDocument();  
  expect(picture).toBeInTheDocument();
  expect(price).toBeInTheDocument();
  expect(quantity).toBeInTheDocument();
});