import React, { useEffect, useState } from 'react';
import './ProductList.css';

import { apiProdutos } from '../../../services/api';
import { useCart } from '../../../context/CartContext';
import Product, { IProduct } from '../Product';

function ProductList() {
  const { addProduct } = useCart();
  const [productsList, setProductsList] = useState<IProduct[]>();

  useEffect(() => {
    const getProducts = async () => {
      const { data } = await apiProdutos.get('/products');
      setProductsList(data);
    }

    getProducts();
  }, []);

  return (      
    <ul className="products">
      {productsList?.map(item => 
        <Product
          className="products__item"
          product={item}
          key={item.id}
          addToCart={(product: object) => addProduct(product)} 
        />)}
    </ul>
  );
}

export default ProductList;