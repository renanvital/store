import React from 'react';
import { useCart } from '../../../context/CartContext';
import './Main.css';

interface Props {
  title: string;
  children: React.ReactNode;
}

function Main(props: Props) {
  const { title, children } = props;
  const { isOpen } = useCart();

  return (      
    <main className={`main ${ isOpen ? 'main--close': ''}`}>
      <h2 className="main__title">{title}</h2>
      {children}
    </main>
  );
}

export default Main;