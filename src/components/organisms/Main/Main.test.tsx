import React from 'react';
import { render } from '@testing-library/react';

import Main from './index';

it('should be able to render main component', () => {
  const { container } = render(
    <Main title="Produtos">
    </Main>
  );  
  
  const classMain = container.firstChild;

  expect(classMain).toHaveClass('main');
});

it('should be able to show a title', () => {
  const { getByText } = render(
    <Main title="Produtos">
    </Main>
  );  

  const title = getByText('Produtos');
  
  expect(title).toBeInTheDocument();  
});

it('should be able render children', () => {
  const { getByText } = render(
    <Main title="Produtos">
      <h1>Hello Leroy Merlin</h1>
    </Main>
  );  

  const headingTitle = getByText('Hello Leroy Merlin');
  
  expect(headingTitle).toBeInTheDocument();
});