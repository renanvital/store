import React from 'react';
import { render } from '@testing-library/react';

import Header from './index';

it('should be able to render header component', () => {
  const { container } = render(<Header />);  
  const classHeader = container.firstChild;

  expect(classHeader).toHaveClass('header');
});

it('should be able to render logo', () => {
  const { getByAltText } = render(<Header />);    
  const logo = getByAltText('Leroy Merlin');

  expect(logo).toBeInTheDocument();
});

it('should be able to render cart', () => {
  const { getByText, getByAltText } = render(<Header />);    
  const text = getByText('Carrinho');
  const icon = getByAltText('Carrinho');

  expect(text).toBeInTheDocument();
  expect(icon).toBeInTheDocument();
});