import React from 'react';
import './Header.css';

import CartHeader from '../../molecules/CartHeader';
import logo from '../../../assets/leroy-merlin.svg';

function Header() {
  return (
    <header className="header">
      <h1 className="header__title">
        <img className="header__logo" src={logo} alt="Leroy Merlin" />
      </h1>
      <CartHeader />
    </header>
  );
}

export default Header;