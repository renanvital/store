import React, { createContext, useContext, useState, useCallback, useEffect } from 'react';
import { apiFrete } from '../services/api';

import { toast } from 'react-toastify';
interface Product {
  id: number;
  name: string;
  picture: string;
  price: string;
  quantity: number;
}
interface Cart {
  isOpen: boolean;
  products: Product[];
  shipping: string;
  totalProducts: number;
  CEP: string;
  subTotal: number;
  subTotalWithShipping: number;
  setCEP: Function;
  openCart: Function;
  closeCart: Function;
  addProduct: Function;
  removeProduct: Function;
  incrementProduct: Function;
  decrementProduct: Function;
  calculateShipping: Function;
  checkout: Function;
}

const CartContext = createContext<Cart>({} as Cart);

const CartProvider: React.FC = ({ children }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [products, setProducts] = useState<Product[]>([]);
  const [shipping, setShipping] = useState<string>('');
  const [CEP, setCEP] = useState<string>('');
  const [totalProducts, setTotalProducts] = useState<number>(0);
  const [subTotal, setSubtotal] = useState<number>(0);
  const [subTotalWithShipping, setSubTotalWithShipping] = useState<number>(0);

  const Toast = useCallback((message, type) => {
    switch (type) {
      case 'success':
        toast.success(message);
        break;
      case 'info':
        toast.info(message);
        break;
      case 'warn':
        toast.warn(message);
        break;
      case 'error':
        toast.error(message);
        break;
      default:
        toast(message);
        break;
    }
  }, []);
  
  const openCart = () => setIsOpen(true);
  const closeCart = () => setIsOpen(false);

  const updateTotal = useCallback(() => {    
    const sum = products.reduce((accumulator, product) => {
      return accumulator + parseFloat(product.price) * product.quantity;
    }, 0);

    setSubtotal(sum);
    setSubTotalWithShipping(sum + parseFloat(shipping));
  },[products, shipping]);
  
  useEffect(() => {
    updateTotal();
  }, [updateTotal]);

  const updateProducts = useCallback((products) => {
    setProducts(products);
    setTotalProducts(products.length);
  },[]);
  
  const findProduct = (id: number) => {
    return products.findIndex(item => item.id === id);
  }
  
  const addProduct = (product: Product) => {
    const index = findProduct(product.id);
    if(index === -1) {      
      updateProducts([...products, product]);
      
      Toast("Produto adicionado ao carrinho!", "success");      
    } else {
      Toast("Esse produto já está no carrinho!", "warn");
    }
  };

  const incrementProduct = (id: number) => {
    const index = findProduct(id);
    const quantity = products[index].quantity;
    
    if(quantity < 10) {
      products[index].quantity++;
      updateProducts([...products]);
    } else {
      Toast("Máximo de 10 items por produto!", "warn");
    }
  }

  const decrementProduct = (id: number) => {
    const index = findProduct(id);
    const quantity = products[index].quantity;
    if(quantity > 1 ) {
      products[index].quantity--;
    }
    updateProducts([...products]);
  }

  const removeProduct = (id: number) => {
    const index = findProduct(id);
    products.splice(index, 1);
    updateProducts([...products]);
  }

  const calculateShipping = async (cep: string) => {
    const { data } = await apiFrete.get(`${cep}`);
    
    if(data.freight) {
      setShipping(data.freight);      
      Toast("Frete calculado com sucesso!", "info");
    }
    else {
      setShipping('');
      Toast("CEP inválido! Aceita apenas o formato 00000-000 ou 00000000", "error");
    }
  };

  const checkout = () => {
    updateProducts([]);
    closeCart();
    Toast("Obrigado por comprar em nossa loja!", "info");
  }

  return (
    <CartContext.Provider value={{
      isOpen,
      products,
      shipping,
      totalProducts,
      CEP,
      setCEP,
      openCart,
      closeCart,
      addProduct,
      removeProduct,
      incrementProduct,
      decrementProduct,
      calculateShipping,
      checkout,
      subTotal,
      subTotalWithShipping,
    }}>
      {children}
    </CartContext.Provider>
  )
}

const useCart = () => {
  const context = useContext(CartContext);
  
  if(!context)
    throw Error('useCart must be use within CartProvider');

  return context;
}

export { useCart, CartProvider }