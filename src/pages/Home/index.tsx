import React from 'react';
import { CartProvider } from '../../context/CartContext';

import Header from '../../components/organisms/Header';
import Main from '../../components/organisms/Main';

import ProductList from '../../components/molecules/ProductList';
import CartList from '../../components/molecules/CartList';

import { ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Home() {
  return (
    <CartProvider>
      <CartList />
      <Header />
      <Main title="Produtos">
        <ProductList />
      </Main>
      <ToastContainer />
    </CartProvider>
  );
}

export default Home;