import axios from 'axios';

const apiFrete = axios.create({
  baseURL: "https://zs5utiv3ul.execute-api.us-east-1.amazonaws.com/dev/freight/",
});

const apiProdutos = axios.create({
  baseURL: "https://zs5utiv3ul.execute-api.us-east-1.amazonaws.com/dev/",
});

export { apiFrete, apiProdutos }