# Leroy Store

<h1 align="center"><a href="https://leroystore.netlify.app/"><img src="./public/Leroy-Store.png" /></a></h1>

<h2 align="center">Loja virtual que simula uma experiência de compra online.</h2>

<p align="center">
  <a href="https://leroystore.netlify.app/" target="blank">Demonstração</a> •
  <a href="#objetivo">Objetivo</a> •
  <a href="#status">Status</a> •
  <a href="#recursos">Recursos</a> • 
  <a href="#tecnologias">Tecnologias e Padrões</a> •
  <a href="#baixar-projeto">Baixar Projeto</a> •
  <a href="#autor">Sobre o Autor</a>
</p>

<hr>

### **Objetivo**

Criar uma **aplicação web com React** sem utilizar bibliotecas de estilização como: Bootstrap, PureCss, MaterialUI e etc. **Usar somente HTML, CSS e JS** ... na unha para **conquistar uma vaga de Front-end** em uma **empresa incrível!**


### **Status**
 **Aplicação concluída com sucesso, aguardando os próximos passos!**

 <hr>

### **Recursos**

- [x] Listagem de produtos
- [x] Carrinho de compras
- [x] Cálculo de frete
- [ ] Loading ...

<hr>

### **Tecnologias**

- [**HTML**](https://developer.mozilla.org/en-US/docs/Web/HTML)  
- [**CSS**](https://developer.mozilla.org/pt-BR/docs/Web/CSS) 
- [**JS**](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript)
- [**React**](https://pt-br.reactjs.org/)
- [**Context API**](https://pt-br.reactjs.org/docs/context.html)
- [**TypeScript**](https://www.typescriptlang.org/)
- [**Conventional Commits**](https://www.conventionalcommits.org/pt-br/v1.0.0-beta.4/)
- [**BEM CSS**](http://getbem.com/)
- [**Atomic Design**](https://bradfrost.com/blog/post/atomic-web-design/)

<hr>

### **Baixar Projeto**

#### Abmbiente de Desenvolvimento

```bash

  # Clonar repositório
  $ git clone https://gitlab.com/renanvital/store.git

  # Entrar no diretório
  $ cd store

  # Instalar dependências
  $ yarn install

  # Iniciar o projeto
  $ yarn start

```

#### Rodar Testes
```bash

  # Build para a produção
  $ yarn test

```

#### Gerar Build

```bash

  # Build para a produção
  $ yarn build

```

<hr>

### Autor
---

<a href="https://gitlab.com/renanvital">
 <img style="border-radius: 50%;" src="https://secure.gravatar.com/avatar/a01a6969149e01fe5fa7c1b094553ef9?s=180&d=identicon" width="100px;" alt=""/>
 </a> 
 
 <a href="https://gitlab.com/renanvital" title="Renan Vital"><b>Renan Vital</b></a>


Feito com ❤️ por Renan Vital 👋🏽 Entre em contato!

[![Linkedin Badge](https://img.shields.io/badge/-Renan-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/renan-vital/)](https://www.linkedin.com/in/renan-vital/) 
[![Gmail Badge](https://img.shields.io/badge/renanbreno@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:renanbreno@gmail.com)](mailto:renanbreno@gmail.com)
